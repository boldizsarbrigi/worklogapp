<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>WorkLog_Logs</title>
</head>
<body>

	<h2>WorkLog List</h2>

	<table border="1">
		<thead>
			<th>Id WorkLog</th>
			<th>Start Date</th>
			<th>End Date</th>
			<th>Employee</th>
			<th>Project</th>
			<th>Delete WorkLog</th>
			<th>Update WorkLog</th>
		</thead>

		<c:forEach items="${listWorkLogs}" var="worklog">
			<tr>
				<td><c:out value="${worklog.getIdWorkLog()}"></c:out></td>
				<fmt:formatDate value="${worklog.startDate}"
					pattern="yyyy-MM-dd HH:mm" var="startDate" />
				<td><c:out value="${startDate}"></c:out></td>
				<fmt:formatDate value="${worklog.endDate}"
					pattern="yyyy-MM-dd HH:mm" var="endDate" />
				<td><c:out value="${endDate}"></c:out></td>
				<td><c:out value="${worklog.getEmployee().getUserName()}"></c:out></td>
				<td><c:out value="${worklog.getProject().getProjectName()}"></c:out></td>


				<td><form method="GET"
						action="${pageContext.request.contextPath}/worklog/deleteWorkLogById">
						<input type="hidden" name="idWorkLog"
							value="${worklog.getIdWorkLog()}"> <input type="Submit"
							value="Delete">
					</form></td>

				<td>
					<form method="GET"
						action="${pageContext.request.contextPath}/worklog/updateWorkLogById">
						<input type="hidden" name="idWorkLog"
							value="${worklog.getIdWorkLog()}"> <input type="Submit"
							value="Update">
					</form>
				</td>
			</tr>
		</c:forEach>
	</table>

	<h3>Add new work-log</h3>
	<form method="POST"
		action="${pageContext.request.contextPath}/worklog/addWorkLog">

		<fmt:formatDate value="${worklog.startDate}"
			pattern="yyyy-MM-dd HH:mm" var="startDate" />
		<input type="text" name="startDate"
			placeholder="StartDate[yyy-MM-dd HH:mm]"
			value="${worklog.getStartDate()}">

		<fmt:formatDate value="${worklog.endDate}" pattern="yyyy-MM-dd HH:mm"
			var="endDate" />
		<input type="text" name="endDate"
			placeholder="EndDate[yyy-MM-dd HH:mm]"
			value="${worklog.getEndDate()}"> 
			
			
			<select name="idEmployee">	
			<c:forEach items="${listEmployees}" var="employee">
				<option value="${employee.getIdEmployee()}">
					<c:out value="${employee.getUserName()}"></c:out>
				</option>
			</c:forEach>
		</select> 
	
		
		<select name="idProject">
			<c:forEach items="${listProjects}" var="project">
				<option value="${project.getIdProject()}">
					<c:out value="${project.getProjectName()}"></c:out>
				</option>
			</c:forEach>
		</select>
		
		<input type="submit"
			value="Add WorkLog"> 


	</form>
</body>
</html>