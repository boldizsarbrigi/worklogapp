<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>WorkLog_Projects</title>
</head>
<body>

	<h2>Projects List</h2>

	<table border="1">
		<thead>
			<th>Id Project</th>
			<th>Project Name</th>
			<th>Project Value(RON)</th>
			<th>Delete Project</th>
			<th>Update Project</th>
		</thead>

		<c:forEach items="${listProjects}" var="project">
			<tr>
				<td><c:out value="${project.getIdProject()}"></c:out></td>
				<td><c:out value="${project.getProjectName()}"></c:out></td>
				<td><c:out value="${project.getProjectValue()}"></c:out></td>

				<td><form method="GET"
						action="${pageContext.request.contextPath}/project/deleteProjectById">
						<input type="hidden" name="idProject"
							value="${project.getIdProject()}"> <input type="Submit"
							value="Delete">
					</form></td>

				<td>
					<form method="GET"
						action="${pageContext.request.contextPath}/project/updateProjectById">
						<input type="hidden" name="idProject"
							value="${project.getIdProject()}"> <input type="Submit"
							value="Update">
					</form>
				</td>
			</tr>
		</c:forEach>
	</table>

	<h3>Add new project</h3>
	<form method="POST"
		action="${pageContext.request.contextPath}/project/addProject">
		<input type="text" name="projectName" placeholder="Project Name"
			value="${project.getProjectName()}"> <input type="text"
			name="projectValue" placeholder="Project Value"
			value="${project.getProjectValue()}"> <input type="submit"
			value="Add Project">
	</form>
</body>
</html>