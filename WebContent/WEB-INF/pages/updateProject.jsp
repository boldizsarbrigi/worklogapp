<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Project update</title>
</head>
<body>
	<h3>Update project</h3>
	<form method="POST"
		action="${pageContext.request.contextPath}/project/updateProjectById">
		<input type="hidden" name="idProject"
			value='${project.getIdProject()}' /> <input type="text"
			name="projectName" value='${project.getProjectName()}' /> <input
			type="text" name="projectValue" value='${project.getProjectValue()}'>
		<input type="submit" value="Update Project">
	</form>
</body>
</html>