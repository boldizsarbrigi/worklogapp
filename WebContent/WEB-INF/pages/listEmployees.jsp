<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>WorkLogApp_Employee</title>
</head>
<body>

	<h2>Employees List</h2>
	<table border="1">
		<thead>
			<th>Id Employee</th>
			<th>First Name</th>
			<th>Last Name</th>
			<th>Email Address</th>
			<th>User Name</th>
			<th>Password</th>
			<th>Salary (RON)</th>
			<th>Delete Employee</th>
			<th>Update Employee</th>
		</thead>
		<c:forEach items="${listEmployees}" var="iterator">
			<tr>
				<td><c:out value="${iterator.getIdEmployee()}"></c:out></td>
				<td><c:out value="${iterator.getFirstName()}"></c:out></td>
				<td><c:out value="${iterator.getLastName()}"></c:out></td>
				<td><c:out value="${iterator.getEmail()}"></c:out></td>
				<td><c:out value="${iterator.getUserName()}"></c:out></td>
				<td><c:out value="${iterator.getPassword()}"></c:out></td>
				<td><c:out value="${iterator.getSalary()}"></c:out></td>

				<td><form method="GET"
						action="${pageContext.request.contextPath}/employee/deleteEmployeeById">
						<input type="hidden" name="idEmployee"
							value="${iterator.getIdEmployee()}"> <input type="Submit"
							value="Delete">
					</form></td>

				<td>
					<form method="GET"
						action="${pageContext.request.contextPath}/employee/updateEmployeeById">
						<input type="hidden" name="idEmployee"
							value="${iterator.getIdEmployee()}"> <input type="Submit"
							value="Update">
					</form>
				</td>
			</tr>
		</c:forEach>
	</table>

	<br>

	<h3>Add new employee</h3>
	<form method="POST"
		action="${pageContext.request.contextPath}/employee/addEmployee">
		<input type="text" name="firstName" placeholder="First Name"
			value="${iterator.getFirstName()}"> <input type="text"
			name="lastName" placeholder="Last Name"
			value="${iterator.getLastName()}"> <input type="text"
			name="email" placeholder="Email Address"
			value="${iterator.getEmail()}"> <input type="text"
			name="userName" placeholder="User Name"
			value="${iterator.getUserName()}"> <input type="text"
			name="password" placeholder="Password"
			value="${iterator.getPassword()}"> <input type="text"
			name="salary" placeholder="Salary" value="${iterator.getSalary()}">
		<input type="submit" value="Add Employee">
	</form>
</body>
</html>