<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>WorkLog update</title>
</head>
<body>
	<h3>Update work-log</h3>
	<form method="POST"
		action="${pageContext.request.contextPath}/worklog/updateWorkLogById">
		<input type="hidden" name="idWorkLog"
			value='${worklog.getIdWorkLog()}' /> 
					
		<fmt:formatDate value="${worklog.startDate}"
			pattern="yyyy-MM-dd HH:mm" var="startDate" />
		<input type="text" name="startDate" value='${worklog.getStartDate()}' />

		<fmt:formatDate value="${worklog.endDate}" pattern="yyyy-MM-dd HH:mm"
			var="endDate" />
		<input type="text" name="endDate" value='${worklog.getEndDate()}'>

		<input type="submit" value="Update WorkLog">
	</form>
</body>
</html>