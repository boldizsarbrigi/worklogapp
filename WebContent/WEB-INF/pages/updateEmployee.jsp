<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Employee update</title>
</head>
<body>
	<h3>Update employee</h3>
	<form method="POST"
		action="${pageContext.request.contextPath}/employee/updateEmployeeById">
		<input type="hidden" name="idEmployee"
			value='${employee.getIdEmployee()}' /> <input type="text"
			name="firstName" value='${employee.getFirstName()}' /> <input
			type="text" name="lastName" value='${employee.getLastName()}'>
		<input type="text" name="email" value='${employee.getEmail()}'>
		<input type="text" name="userName" value='${employee.getUserName()}'>
		<input type="text" name="password" value='${employee.getPassword()}'>
		<input type="text" name="salary" value='${employee.getSalary()}'>
		<input type="submit" value="Update Employee">
	</form>

</body>
</html>