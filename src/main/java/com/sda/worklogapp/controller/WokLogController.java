package com.sda.worklogapp.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.sda.worklogapp.model.Employee;
import com.sda.worklogapp.model.Project;
import com.sda.worklogapp.model.WorkLog;
import com.sda.worklogapp.service.EmployeeService;
import com.sda.worklogapp.service.ProjectService;
import com.sda.worklogapp.service.WorkLogService;

//URL: http://localhost:8080/WorkLogApp/worklog/getAllWorkLogs
@Controller
@RequestMapping("/worklog")
public class WokLogController {

	@Autowired
	private WorkLogService workLogService;
	@Autowired
	private EmployeeService employeeService;
	@Autowired
	private ProjectService projectService;

	@RequestMapping(value = "/getAllWorkLogs", method = RequestMethod.GET)
	public String getAllWorkLogs(ModelMap model) {
		model.addAttribute("listWorkLogs", workLogService.listAllWorkLogs());
		model.addAttribute("listEmployees",employeeService.listAllEmployees());
		model.addAttribute("listProjects", projectService.listAllProjects());
		return "listWorkLogs";
	}

	@RequestMapping(value = "/deleteWorkLogById", method = RequestMethod.GET)
	public String deleteWorkLogById(@RequestParam("idWorkLog") int idWorkLog, ModelMap model) {
		workLogService.deleteWorkLogById(idWorkLog);
		model.addAttribute("listWorkLogs", workLogService.listAllWorkLogs());
		return "listWorkLogs";
	}

	@RequestMapping(value = "/updateWorkLogById", method = RequestMethod.GET)
	public String updateWorkLogById(@RequestParam("idWorkLog") int idWorkLog, ModelMap model) {
		model.addAttribute("worklog", workLogService.getWorkLogById(idWorkLog));
		return "updateWorkLog";
	}

	@RequestMapping(value = "/updateWorkLogById", method = RequestMethod.POST)
	public String updateWorkLogById(@ModelAttribute WorkLog workLog, ModelMap model) {
		workLogService.addWorkLog(workLog);
		model.addAttribute("listWorkLogs", workLogService.listAllWorkLogs());
		return "listWorkLogs";
	}

	@RequestMapping(value = "/addWorkLog", method = RequestMethod.POST)
	public String addWorkLog(@RequestParam("startDate") Date startDate, @RequestParam("endDate") Date endDate,
			@RequestParam("idEmployee") int idEmployee, @RequestParam("idProject") int idProject, ModelMap model) {
		Employee employee= new Employee(idEmployee);
		Project project= new Project(idProject);
		WorkLog workLog = new WorkLog(startDate, endDate);
		workLog.setEmployee(employee);
		workLog.setProject(project);
		workLogService.addWorkLog(workLog);
		model.addAttribute("listWorkLogs", workLogService.listAllWorkLogs());
		return "listWorkLogs";
	}

	@InitBinder
	private void dateBinder(WebDataBinder binder) {
		// The date format to parse or output your dates
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		// Create a new CustomDateEditor
		CustomDateEditor editor = new CustomDateEditor(dateFormat, true);
		// Register it as custom editor for the Date type
		binder.registerCustomEditor(Date.class, editor);
	}

}
