package com.sda.worklogapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import com.sda.worklogapp.model.Project;
import com.sda.worklogapp.service.ProjectService;

//URL: http://localhost:8080/WorkLogApp/project/getAllProjects
@Controller
@RequestMapping("/project")
public class ProjectController {

	@Autowired
	private ProjectService projectService;

	@RequestMapping(value = "/getAllProjects", method = RequestMethod.GET)
	public String getAllProjects(ModelMap model) {
		model.addAttribute("listProjects", projectService.listAllProjects());
		return "listProjects";
	}

	@RequestMapping(value = "/deleteProjectById", method = RequestMethod.GET)
	public String deleteProjectById(@RequestParam("idProject") int idProject, ModelMap model) {
		projectService.deleteProjectById(idProject);
		model.addAttribute("listProjects", projectService.listAllProjects());
		return "listProjects";
	}

	@RequestMapping(value = "/updateProjectById", method = RequestMethod.GET)
	public String updateProjectById(@RequestParam("idProject") Integer idProject, ModelMap model) {
		model.addAttribute("project", projectService.getProjectById(idProject));
		return "updateProject";
	}

	@RequestMapping(value = "/updateProjectById", method = RequestMethod.POST)
	public String updateProjectById(@ModelAttribute Project project, ModelMap model) {
		projectService.updateProject(project);
		model.addAttribute("listProjects", projectService.listAllProjects());
		return "listProjects";
	}

	@RequestMapping(value = "/addProject", method = RequestMethod.POST)
	public String addProject(@RequestParam("projectName") String projectName,
			@RequestParam("projectValue") long projectValue, ModelMap model) {
		Project project = new Project(projectName, projectValue);
		projectService.addProject(project);
		model.addAttribute("listProjects", projectService.listAllProjects());
		return "listProjects";
	}

}
