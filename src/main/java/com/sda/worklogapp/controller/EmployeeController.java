package com.sda.worklogapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import com.sda.worklogapp.model.Employee;
import com.sda.worklogapp.service.EmployeeService;

// URL: http://localhost:8080/WorkLogApp/employee/getAllEmployee
@Controller
@RequestMapping("/employee")
public class EmployeeController {

	@Autowired
	private EmployeeService employeeService;

	@RequestMapping(value = "/getAllEmployee", method = RequestMethod.GET)
	public String getEmployeeList(ModelMap model) {
		model.addAttribute("listEmployees", employeeService.listAllEmployees());
		return "listEmployees";
	}

	@RequestMapping(value = "/deleteEmployeeById", method = RequestMethod.GET)
	public String deleteEmployeeById(@RequestParam("idEmployee") Integer idEmployee, ModelMap model) {
		employeeService.deleteEmployeeById(idEmployee);
		model.addAttribute("listEmployees", employeeService.listAllEmployees());
		return "listEmployees";
	}
	
	@RequestMapping(value = "/updateEmployeeById", method = RequestMethod.GET)
	public String updateEmployeeById(@RequestParam("idEmployee") Integer idEmployee, ModelMap model) {
		model.addAttribute("employee", employeeService.getEmployeeById(idEmployee));
		return "updateEmployee";
	}
	
	@RequestMapping(value = "/updateEmployeeById", method = RequestMethod.POST)
	public String updateEmployeeById(@ModelAttribute Employee employee, ModelMap model) {
		employeeService.updateEmployee(employee);
		model.addAttribute("listEmployees", employeeService.listAllEmployees());
		return "listEmployees";
	}

	@RequestMapping(value = "/addEmployee", method = RequestMethod.POST)
	public String addEmployee(@RequestParam("firstName") String firstName, @RequestParam("lastName") String lastName,
			@RequestParam("email") String email, @RequestParam("userName") String userName,
			@RequestParam("password") String password, @RequestParam("salary") int salary, ModelMap model) {
		Employee employee = new Employee(firstName, lastName, email, userName, password, salary);
		employeeService.addEmployee(employee);
		model.addAttribute("listEmployees", employeeService.listAllEmployees());
		return "listEmployees";
	}

}
