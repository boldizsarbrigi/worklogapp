package com.sda.worklogapp.model;


import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "work_log")
public class WorkLog {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idWorkLog;
	private Date startDate;
	private Date endDate;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "idEmployee")
	private Employee employee;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "idProject")
	private Project project;

	public WorkLog() {
	}

	public WorkLog(Date startDate, Date endDate) {
		this.startDate = startDate;
		this.endDate = endDate;
	}

	public WorkLog(Date startDate, Date endDate, Employee employee, Project project) {
		super();
		this.startDate = startDate;
		this.endDate = endDate;
		this.employee = employee;
		this.project = project;
	}

	public WorkLog(int idWorkLog, Date startDate, Date endDate) {
		this.idWorkLog = idWorkLog;
		this.startDate = startDate;
		this.endDate = endDate;
	}

	public int getIdWorkLog() {
		return idWorkLog;
	}

	public void setIdWorkLog(int idWorkLog) {
		this.idWorkLog = idWorkLog;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	@Override
	public String toString() {
		return "WorkLog [idWorkLog=" + idWorkLog + ", startDate=" + startDate + ", endDate=" + endDate + ", employee="
				+ employee + ", project=" + project + "]";
	}

}
