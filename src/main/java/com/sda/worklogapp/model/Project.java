package com.sda.worklogapp.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "project")
public class Project {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idProject;
	private String projectName;
	private long projectValue;

	@ManyToMany(cascade = CascadeType.ALL, mappedBy = "projects")
	private List<Employee> employees;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "project")
	private List<WorkLog> workLogs;

	public Project() {
	}

	public Project(int idProject, String projectName, long projectValue) {
		this.idProject = idProject;
		this.projectName = projectName;
		this.projectValue = projectValue;
	}

	public Project(String projectName, long projectValue) {
		this.projectName = projectName;
		this.projectValue = projectValue;
	}

	public Project(int idProject) {
		this.idProject = idProject;
	}

	public List<Employee> getEmployees() {
		return employees;
	}

	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}

	public List<WorkLog> getWorkLogs() {
		return workLogs;
	}

	public void setWorkLogs(List<WorkLog> workLogs) {
		this.workLogs = workLogs;
	}

	public int getIdProject() {
		return idProject;
	}

	public void setIdProject(int idProject) {
		this.idProject = idProject;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public long getProjectValue() {
		return projectValue;
	}

	public void setProjectValue(long projectValue) {
		this.projectValue = projectValue;
	}

	@Override
	public String toString() {
		return "Project [idProject=" + idProject + ", projectName=" + projectName + ", projectValue=" + projectValue
				+ ", employees=" + employees + ", workLogs=" + workLogs + "]";
	}

}
