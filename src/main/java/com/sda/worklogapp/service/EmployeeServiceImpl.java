package com.sda.worklogapp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sda.worklogapp.dao.EmployeeDao;
import com.sda.worklogapp.model.Employee;

@Service
@Transactional
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	private EmployeeDao employeeDao;

	@Override
	public void addEmployee(Employee e) {
		employeeDao.addEmployee(e);

	}

	@Override
	public void deleteEmployee(Employee e) {
		employeeDao.deleteEmployee(e);
	}

	@Override
	public List<Employee> listAllEmployees() {
		return employeeDao.listAllEmployees();
	}

	@Override
	public void deleteEmployeeById(int idEmployee) {
		employeeDao.deleteEmployeeById(idEmployee);

	}

	@Override
	public void updateEmployee(Employee updateEmployee) {
		employeeDao.updateEmployee(updateEmployee);

	}

	@Override
	public Employee getEmployeeById(int idEmployee) {	
		return employeeDao.getEmployeeById(idEmployee);
	}

}
