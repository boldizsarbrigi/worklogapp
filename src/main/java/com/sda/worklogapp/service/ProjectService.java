package com.sda.worklogapp.service;

import java.util.List;

import com.sda.worklogapp.model.Project;

public interface ProjectService {

	public void addProject(Project p);

	public void deleteProject(Project p);

	public List<Project> listAllProjects();

	public void deleteProjectById(int idProject);

	public void updateProject(Project updateProject);

	public Project getProjectById(int idProject);

}
