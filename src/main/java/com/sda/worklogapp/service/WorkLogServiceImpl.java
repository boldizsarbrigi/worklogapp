package com.sda.worklogapp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sda.worklogapp.dao.WorkLogDao;
import com.sda.worklogapp.model.WorkLog;

@Service
@Transactional
public class WorkLogServiceImpl implements WorkLogService {

	@Autowired
	private WorkLogDao workLogDao;

	@Override
	public void addWorkLog(WorkLog w) {
		workLogDao.addWorkLog(w);
	}

	@Override
	public void deleteWorkLog(WorkLog w) {
		workLogDao.deleteWorkLog(w);
	}

	@Override
	public List<WorkLog> listAllWorkLogs() {
		return workLogDao.listAllWorkLogs();
	}

	@Override
	public void deleteWorkLogById(int idWorkLog) {
		workLogDao.deleteWorkLogById(idWorkLog);
	}

	@Override
	public void updateWorkLogById(WorkLog updateWorkLog) {
		workLogDao.updateWorkLogById(updateWorkLog);
	}

	@Override
	public WorkLog getWorkLogById(int idWorkLog) {
		return workLogDao.getWorkLogById(idWorkLog);
	}

}
