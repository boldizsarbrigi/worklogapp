package com.sda.worklogapp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sda.worklogapp.dao.ProjectDao;
import com.sda.worklogapp.model.Project;

@Service
@Transactional
public class ProjectServiceImpl implements ProjectService {

	@Autowired
	private ProjectDao projectDao;

	@Override
	public void addProject(Project p) {
		projectDao.addProject(p);

	}

	@Override
	public void deleteProject(Project p) {
		projectDao.deleteProject(p);

	}

	@Override
	public List<Project> listAllProjects() {
		return projectDao.listAllProjects();
	}

	@Override
	public void deleteProjectById(int idProject) {
		projectDao.deleteProjectById(idProject);

	}

	@Override
	public void updateProject(Project updateProject) {
		projectDao.updateProject(updateProject);

	}

	@Override
	public Project getProjectById(int idProject) {
		return projectDao.getProjectById(idProject);
	}

}
