package com.sda.worklogapp.dao;

import java.util.List;

import com.sda.worklogapp.model.Employee;

public interface EmployeeDao {

	public void addEmployee(Employee e);

	public void deleteEmployee(Employee e);

	public List<Employee> listAllEmployees();

	public void deleteEmployeeById(int idEmployee);

	public void updateEmployee(Employee updateEmployee);

	public Employee getEmployeeById(int idEmployee);
}
