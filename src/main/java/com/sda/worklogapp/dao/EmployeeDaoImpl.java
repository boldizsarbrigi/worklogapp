package com.sda.worklogapp.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.sda.worklogapp.model.Employee;

@Repository
public class EmployeeDaoImpl implements EmployeeDao {

	@Autowired
	private SessionFactory sessionFactory;

	public void addEmployee(Employee e) {
		Session session = sessionFactory.getCurrentSession();
		session.save(e);
	}

	@Override
	public void deleteEmployee(Employee e) {
		Session session = sessionFactory.getCurrentSession();
		session.delete(e);
	}

	public List<Employee> listAllEmployees() {
		Session session = sessionFactory.getCurrentSession();
		List<Employee> employees = session.createQuery("from Employee").list();
		return employees;
	}

	@Override
	public void deleteEmployeeById(int idEmployee) {
		Session session = sessionFactory.getCurrentSession();
		Employee e = new Employee();
		e.setIdEmployee(idEmployee);
		session.delete(e);
	}

	@Override
	public void updateEmployee(Employee updateEmployee) {
		Session session = sessionFactory.getCurrentSession();
		session.update(updateEmployee);
	}

	@Override
	public Employee getEmployeeById(int idEmployee) {
		Session session = sessionFactory.getCurrentSession();
		Employee employee = (Employee) session.get(Employee.class, idEmployee);
		return employee;
	}

}
