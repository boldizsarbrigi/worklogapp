package com.sda.worklogapp.dao;

import java.util.List;

import com.sda.worklogapp.model.WorkLog;

public interface WorkLogDao {

	public void addWorkLog(WorkLog w);

	public void deleteWorkLog(WorkLog w);

	public List<WorkLog> listAllWorkLogs();

	public void deleteWorkLogById(int idWorkLog);

	public void updateWorkLogById(WorkLog updateWorkLog);
	
	public WorkLog getWorkLogById(int idWorkLog);

}
