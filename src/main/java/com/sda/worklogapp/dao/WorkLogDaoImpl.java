package com.sda.worklogapp.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sda.worklogapp.model.WorkLog;

@Repository
public class WorkLogDaoImpl implements WorkLogDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void addWorkLog(WorkLog w) {
		Session session = sessionFactory.getCurrentSession();
		session.save(w);
	}

	@Override
	public void deleteWorkLog(WorkLog w) {
		Session session = sessionFactory.getCurrentSession();
		session.delete(w);
	}

	@Override
	public List<WorkLog> listAllWorkLogs() {
		Session session = sessionFactory.getCurrentSession();
		List<WorkLog> workLogs = session.createQuery("from WorkLog").list();
		return workLogs;
	}

	@Override
	public void deleteWorkLogById(int idWorkLog) {
		Session session = sessionFactory.getCurrentSession();
		WorkLog w = new WorkLog();
		w.setIdWorkLog(idWorkLog);
		session.delete(w);
	}

	@Override
	public void updateWorkLogById(WorkLog updateWorkLog) {
		Session session = sessionFactory.getCurrentSession();
		session.update(updateWorkLog);
	}

	@Override
	public WorkLog getWorkLogById(int idWorkLog) {
		Session session=sessionFactory.getCurrentSession();
		WorkLog workLog=(WorkLog) session.get(WorkLog.class, idWorkLog);
		return workLog;
	}

}
