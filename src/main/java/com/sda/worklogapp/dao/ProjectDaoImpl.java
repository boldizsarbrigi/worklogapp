package com.sda.worklogapp.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sda.worklogapp.model.Project;

@Repository
public class ProjectDaoImpl implements ProjectDao {

	@Autowired
	private SessionFactory sessionFactory;

	public void addProject(Project p) {
		Session session = sessionFactory.getCurrentSession();
		session.save(p);
	}

	public void deleteProject(Project p) {
		Session session = sessionFactory.getCurrentSession();
		session.delete(p);
	}

	public List<Project> listAllProjects() {
		Session session = sessionFactory.getCurrentSession();
		List<Project> projects = session.createQuery("from Project").list();
		return projects;
	}

	public void deleteProjectById(int idProject) {
		Session session = sessionFactory.getCurrentSession();
		Project p = new Project();
		p.setIdProject(idProject);
		session.delete(p);

	}

	public void updateProject(Project updateProject) {
		Session session = sessionFactory.getCurrentSession();
		session.update(updateProject);
	}

	@Override
	public Project getProjectById(int idProject) {
		Session session = sessionFactory.getCurrentSession();
		Project project = (Project) session.get(Project.class, idProject);
		return project;
	}

}
